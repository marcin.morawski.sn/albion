[general]
name=Albion
qgisMinimumVersion=3.2
description=Build 3D geological model from wells information
version=2.5.0-alpha4
author=Oslandia
email=infos@oslandia.com
experimental=False
deprecated=False
icon=icons/albion_logo_icon_no_contour.svg
about=Original idea by Emmanuel Duguey.
	Plugin development funded by Orano Mining
repository=https://gitlab.com/Oslandia/albion.git
tracker=https://gitlab.com/Oslandia/albion/-/issues

